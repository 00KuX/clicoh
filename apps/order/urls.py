from rest_framework import routers
from .viewsets import OrderViewSet,OrderDetailViewSet

routers = routers.SimpleRouter()
routers.register('orders', OrderViewSet, basename="orders")
routers.register('order_detail', OrderDetailViewSet, basename="ordenDetail")


urlpatterns = routers.urls
