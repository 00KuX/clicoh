from gc import get_objects
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action

from .models import Order, OrderDetail
from apps.product.models import Product
from .serializer import OrderSerializer,OrderDetailSerializer

from django.http import Http404

from apps.product.viewsets import ProductStockViewSet

import requests


class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer

    def list(self, request):
        detail_serializer = self.get_serializer(self.get_queryset(), many=True)
        data = {
            
            "rows": detail_serializer.data
        }
        return Response(data, status=status.HTTP_200_OK)

    def get_queryset(self, pk=None):
        if pk is None:
            return self.get_serializer().Meta.model.objects.all()
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message': 'Producto creado correctamente!'}, status=status.HTTP_201_CREATED)
        return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        order = self.get_queryset(pk)
        if order:
            detail = OrderDetail.objects.filter(order__id=order.id)
            object_serializer = OrderDetailSerializer(detail,many=True)
            return Response(object_serializer.data, status=status.HTTP_200_OK)
        return Response({'error':'No existe un Producto con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        if self.get_queryset(pk):
            product_serializer = self.serializer_class(self.get_queryset(pk), data=request.data, partial=True)            
            if product_serializer.is_valid():
                product_serializer.save()
                return Response({'message':'Producto actualizado correctamente!'}, status=status.HTTP_200_OK)
            return Response({'message':'', 'error':product_serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
    
    
    def destroy(self, request, *args, **kwargs):
        
        try:
            instance = self.get_object()
            detail = OrderDetail.objects.filter(order_id=instance.id)
            if detail:
                for i in detail:
                    product = Product.objects.get(id=i.product.id)
                    cuantity = i.cuantity
                    product.stock = product.stock + cuantity
                    product.save()
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Http404:
            return Response({'error':'No existe un detalle de orden con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['get'], detail=True)
    def get_total(self, request, *args, **kwargs):
        id = kwargs.get('pk')
        detail = OrderDetail.objects.filter(order__id=id)
        if detail:    
            total = sum((j.product.price*j.cuantity or 0) for j in detail)
            data = {
                "total": str(total)
            }
            return Response(data, status=status.HTTP_200_OK)
        
        return Response({'error':'No existe detalle o número de orden'}, status=status.HTTP_400_BAD_REQUEST)


    @action(methods=['get'], detail=True)
    def get_total_usd(self, request, *args, **kwargs):
        response = requests.get('https://www.dolarsi.com/api/api.php?type=valoresprincipales')
        if response.status_code == 200:
            data = response.json()
            price_blue = float(str(data[1].get('casa').get('venta')).replace(',','.'))            
            id = kwargs.get('pk')
            detail = OrderDetail.objects.filter(order__id=id)
            if detail:    
                total = sum((j.product.price*j.cuantity or 0) for j in detail)
                total_usd = round(total/price_blue,2)
                data = {
                "total_usd_blue": str(total_usd)
                }
            return Response(data, status=status.HTTP_200_OK)




class OrderDetailViewSet(viewsets.ModelViewSet):

    serializer_class = OrderDetailSerializer

    def get_queryset(self, pk=None):
        if pk is None:
            return self.get_serializer().Meta.model.objects.all()
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def list(self, request):
        detail_serializer = self.get_serializer(self.get_queryset(), many=True)
        data = {
            
            "rows": detail_serializer.data
        }
        return Response(data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            product = Product.objects.get(id=request.data['product'])
            if product.stock > int(request.data['cuantity']):
                serializer.save()
                product.stock = product.stock - int(request.data['cuantity'])
                product.save()
                return Response({'message': 'El detalle de orden fue creado correctamente!'}, status=status.HTTP_201_CREATED)
            else:
                return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)        
        else:
            return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        order = self.get_queryset(pk)
        if order:
            product_serializer = OrderDetailSerializer(order)
            return Response(product_serializer.data, status=status.HTTP_200_OK)
        return Response({'error':'No existe un detalle de orden con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        product = Product.objects.get(id=self.get_object().product.id)
        if self.get_queryset(pk):
            object_serializer = self.serializer_class(self.get_queryset(pk), data=request.data, partial=True)            
            if object_serializer.is_valid():
                object_serializer.save()
                product.stock = product.stock - int(request.data['cuantity'])
                product.save()
                return Response({'message':'El detalle de orden fue actualizado correctamente!'}, status=status.HTTP_200_OK)
            return Response({'message':'', 'error':object_serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):

        product = Product.objects.get(id=self.get_object().product.id)
        try:
            instance = self.get_object()
            cuantity = instance.cuantity
            self.perform_destroy(instance)
            product.stock = product.stock + cuantity
            product.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Http404:
            return Response({'error':'No existe un detalle de orden con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)