from itertools import product
from rest_framework import serializers
from .models import Order, OrderDetail

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['id']
        extra_kwargs = {"id": {"error_messages": {"required": "Give yourself a username"}}}


class OrderDetailSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = OrderDetail
        fields = '__all__'

    def validate_cuantity(self, value): 
        if value < 0:
            raise serializers.ValidationError("No es posible ingresar una cantidad negativa.")
        # elif value > self.instance.product.stock:
        #     raise serializers.ValidationError("¡Error! Cantidad Disponible %s." %(self.instance.product.stock))
        return value    

    def to_representation(self,instance):
        return {
            'id': instance.id,
            'order': instance.order.id,
            'cuantity': instance.cuantity if instance.cuantity is not None else '',
            'product': instance.product.name if instance.product is not None else '',
        }

