from django.db import models

from apps.product.models import Product

class Order(models.Model):
    date_time = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")

    def __str__(self) -> str:
        return "%s" %(self.id)

class OrderDetail(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, verbose_name="Nro de Orden")
    cuantity = models.IntegerField(verbose_name="Cantidad")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name="Producto")

    def __str__(self):
        return str(self.cuantity)


# Create your models here.
