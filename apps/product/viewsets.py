from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

from .serializer import ProductSerializer, ProductRetrieveSerializer, ProductStockSerializer

from django.http import Http404


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer

    def get_queryset(self, pk=None):
        if pk is None:
            return self.get_serializer().Meta.model.objects.all()
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def list(self, request):
        product_serializer = self.get_serializer(self.get_queryset(), many=True)
        data = {
            "total": self.get_queryset().count(),
            "totalNotFiltered": self.get_queryset().count(),
            "rows": product_serializer.data
        }
        return Response(data, status=status.HTTP_200_OK)

    
    def retrieve(self, request, pk=None):
        product = self.get_queryset(pk)
        if product:
            product_serializer = ProductRetrieveSerializer(product)
            return Response(product_serializer.data, status=status.HTTP_200_OK)
        return Response({'error':'No existe un Producto con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)



    def create(self, request):
        serializer = self.serializer_class(data=request.data)     
        if serializer.is_valid():
            serializer.save()
            return Response({'message': 'Producto creado correctamente!'}, status=status.HTTP_201_CREATED)
        return Response({'message':'', 'error':serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None, cuantity=None):
        if self.get_queryset(pk):
            product_serializer = self.serializer_class(self.get_queryset(pk), data=request.data, partial=True)            
            if product_serializer.is_valid():
                product_serializer.save()
                return Response({'message':'Producto actualizado correctamente!'}, status=status.HTTP_200_OK)
            return Response({'message':'', 'error':product_serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    """
    La siguiente función elimina el objeto, lo ideal seria cambiar un estado en el modelo, 
    se realizo de esta manera porque en la consigna no habia un campo llamado por ejemplo:
    'estado' de tipo boolean.
    """

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Http404:
            return Response({'error':'No existe un Producto con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)
        

class ProductStockViewSet(viewsets.ModelViewSet):
    serializer_class = ProductStockSerializer

    def get_queryset(self, pk=None):
        if pk is None:
            return None
        return self.get_serializer().Meta.model.objects.filter(id=pk).first()

    def list(self, request):
        if self.get_queryset():
            product_serializer = self.get_serializer(self.get_queryset(), many=True)
            data = {
                "row": product_serializer.data
            }
            return Response(data, status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({'error':'No existe un Producto con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        product = self.get_queryset(pk)
        if product:
            product_serializer = ProductRetrieveSerializer(product)
            return Response(product_serializer.data, status=status.HTTP_200_OK)
        return Response({'error':'No existe un Producto con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)


    def update(self, request, pk=None, cuantity=None):
        product = self.get_queryset(pk)        
        if product:
            if cuantity:
                product.stock = product.stock + cuantity
            else:
                product.stock = request.data['stock']
            product.save()
            return Response({'message':'Producto actualizado correctamente!'}, status=status.HTTP_200_OK)
        return Response({'error':'No existe un Producto con estos datos!'}, status=status.HTTP_400_BAD_REQUEST)

