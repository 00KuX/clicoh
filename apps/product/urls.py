from rest_framework import routers
from .viewsets import ProductViewSet, ProductStockViewSet

routers = routers.SimpleRouter()
routers.register('products', ProductViewSet, basename="product")
routers.register('productStock', ProductStockViewSet, basename="product")



urlpatterns = routers.urls
