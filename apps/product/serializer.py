from dataclasses import fields
from rest_framework import serializers
from .models import Product

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ProductRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ()

    def to_representation(self,instance):
        return {
            'id' : instance.id,
            'name' : instance.name,
            'price' : instance.price,
            'stock' : instance.stock
        }

class ProductStockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['stock']