from django.db import models

class Product(models.Model):
    id = models.CharField(primary_key=True, max_length=50)
    name = models.CharField(max_length=150, verbose_name="Nombre")
    price = models.FloatField(verbose_name="Precio")
    stock = models.IntegerField(verbose_name="Stock")

    def __str__(self) -> str:
        return "%s - %s" %(self.id, self.name)

# Create your models here.
