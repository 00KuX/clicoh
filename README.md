# clicOH

1 - Clonar repositorio

2 - Crear entorno virtual $ virtualenv --python=python3.7 env

3 - Iniciar entorno virtual $ source env/bin/activate

4 - Instalar requirements $ pip install -r requirements.txt

5 - Crear database $ python manage.py migrate

6 - Iniciar servidor $ python manage.py runserver